FROM httpd:2.4

RUN apt-get -y update
RUN apt-get -y install vim
RUN apt-get -y install libapache2-mod-proxy-html
RUN apt-get -y install libxml2-dev

# copy html files
COPY ./public-html/ /usr/local/apache2/htdocs/
# copy conf.d stuff
RUN echo "Include conf.d/*_*.conf" >> /usr/local/apache2/conf/httpd.conf
COPY ./conf.d /usr/local/apache2/conf.d

# copy SSL stuff